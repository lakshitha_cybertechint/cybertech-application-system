<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('frontend.apply.index');
//    return view('welcome');
});

// Application Submit Routes
Route::group(['namespace' => 'Frontend', 'as' => 'frontend.'], function () {
    Route::resource('apply', 'ApplyController');
});

