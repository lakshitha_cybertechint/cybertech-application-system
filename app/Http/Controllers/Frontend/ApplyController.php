<?php

namespace App\Http\Controllers\Frontend;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Detail;
use App\Models\Account;
use App\Models\Position;
use App\Mail\ApplicationEmail;
use App\Http\Requests\Request;
use App\Http\Requests\ApplyRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;


class ApplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.index')->with(['positions' => Position::where('status', 1)->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ApplyRequest $request)
    {
        $firt_name = null;
        $last_name = null;
        $email = null;
        $file = null;

        if ($request->has('first_name')) {

            // Get Personal Details
            $first_name = $request->input('first_name');
            $last_name = $request->input('last_name');
            $email = $request->input('email');
            $address = $request->input('address');
            $mobile = $request->input('mobile');
            $dob = $request->input('dob');
            $nic = $request->input('nic');
            $cv = null;

            // CV Uploader
            if ($request->hasFile('cv')) {
                $file = $request->file('cv');

                $filename = $first_name . '-' . $last_name . '-' . $request->input('position') . '-' . Carbon::now();

                $filename = strtolower(str_slug($filename)) . '.' . $file->getClientOriginalExtension();

                $path = '/uploads/';

                $file->move(public_path() . $path, $filename);

                $cv = $path . $filename;
            }

            // Save Personal Details
            $user = new User();

            $user->first_name = $first_name;
            $user->last_name = $last_name;
            $user->email = $email;
            $user->address = $address;
            $user->mobile = $mobile;
            $user->dob = $dob;
            $user->nic = $nic;
            $user->cv = $cv;

            $user->save();

            // Get Pro Details
            $last_company = $request->input('last_company');
            $position = $request->input('position');
            $last_position = $request->input('last_position');
            $last_salary = $request->input('last_salary');
            $experience = $request->input('experience');
            $expertise_areas = $request->input('expertise_areas');

            // Save Pro Details
            $detail = new Detail();

            $detail->user_id = $user->id;
            $detail->last_company = $last_company;
            $detail->position = $position;
            $detail->last_position = $last_position;
            $detail->last_salary = $last_salary;
            $detail->experience = $experience;
            $detail->expertise_areas = $expertise_areas;

            $detail->save();

            // Get Bank Details
            $account_number = $request->input('account_number');
            $account_name = $request->input('account_name');
            $bank_name = $request->input('bank_name');
            $branch = $request->input('branch');
            $confirmed = $request->input('confirmed');

            // Save Bank Details
            $account = new Account();

            $account->user_id = $user->id;
            $account->account_number = $account_number;
            $account->account_name = $account_name;
            $account->bank_name = $bank_name;
            $account->branch = $branch;
            $account->confirmed = $confirmed;

            $account->save();
        }

        // Set domain url + file download link
        $file = url('/') . $cv;

        // Sending Mail
        Mail::to('apply@teamcybertech.com')->send(new ApplicationEmail($user, $detail, $account, $file));

        return redirect()->route('frontend.apply.index')->with(['positions' => Position::where('status', 1)->get(), 'flash_success' => 'You have been Successfully Submitted Your Details, Thank You!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
