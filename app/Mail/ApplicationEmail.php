<?php

namespace App\Mail;

use App\Models\User;
use App\Models\Detail;
use App\Models\Account;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApplicationEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $user, $detail, $account, $file;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Detail $detail, Account $account, $file)
    {
        $this->user = $user;
        $this->detail = $detail;
        $this->account = $account;
        $this->file = $file;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = 'New CV From ' . $this->user->first_name . ' to ' . $this->detail->position;

        return $this->from($this->user->email)
            ->markdown('emails.send')
            ->subject($subject)
            ->with(['user' => $this->user, 'detail' => $this->detail, 'account' => $this->account, 'file' => $this->file]);
    }
}
