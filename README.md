# Cybertech Application System

### About

Cybertech web form application is developed to collect user information through a simple laravel web form.

* version 1.0

### How to use(local)

* use homestead to local development
* clone the git repo
* run composer install in the project directory
* create a database called 'cybertech_db'
* update database credentials in env file
* run the application

### How to use(live)

* clone the git repo
* run composer install in the project directory
* more details will provide

### Sending Email(demo)

* Setup a mailtrap account
* Update the email credentials in .env file
* then run php artisan config:cache

### Technologies

* Laravel Framework
* MySQL
* Material Design Bootstrap

### Issues

* Contact the owner/developer of the repo

### License

Application licensed under the [Ovindu Shamal](http://ovindushamal.com/).
